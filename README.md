# Freedomain Podcasts Database

This is an archive of the database that I use for https://freedomainplaylists.com/ .

It contains all the podcast data that is available through the FDRPodcasts API and also some additional  information that I have collected and added on, including: playlist names, freedomain videos that don't have a podcast version, documentaries, and books. (All of these are located in the main PODCASTS table with a podcast number of 10,000+)

In this repo you will also find "**Youtube Video Transcripts**" folder which contains almost all the automatically generated closed captions from the youtube videos before Stefan was banned.

To view the data you can use the fantastic sqlitebrowser: https://github.com/sqlitebrowser/sqlitebrowser

![Preview](https://gitlab.com/fdr_yuriy/freedomain-podcasts-database/uploads/2426148a3c0ee09cf09b792ab79356cb/20220120_213126.jpg)`

## To-Do List

- Append all youtube closed captions to the matching podcast

- Add example python code for referencing the data programatically

- 

{
  "Title": "Why Does Everyone Pretend There's A 'Spanking Debate'?",
  "Id": "NvCZ0hSHxCM",
  "Transcript": [
    {
      "text": "Why Does Everyone Pretend There's A 'Spanking\nDebate'?",
      "start": 4.23,
      "duration": 6.8
    },
    {
      "text": "Spanking was a subject of debate on every\nparenting website on the continent during",
      "start": 11.03,
      "duration": 3.31
    },
    {
      "text": "the past week, and I don't understand why.",
      "start": 14.34,
      "duration": 3.41
    },
    {
      "text": "Yes, I know why it was a topic of conversation\n-- the prestigious journal Pediatrics released",
      "start": 17.75,
      "duration": 6.589
    },
    {
      "text": "a study early in the week showing a possible\nlink between childhood spanking and mental",
      "start": 24.339,
      "duration": 4.961
    },
    {
      "text": "health struggles later in that child's life,\nand that was news worth talking about.",
      "start": 29.3,
      "duration": 5.8
    },
    {
      "text": "What I don't understand is why it was a debate.",
      "start": 35.1,
      "duration": 5.25
    },
    {
      "text": "By definition, that would require two sides.",
      "start": 40.35,
      "duration": 3.75
    },
    {
      "text": "I see only one.",
      "start": 44.1,
      "duration": 3.63
    },
    {
      "text": "At what point does something become simple\nfact?",
      "start": 47.73,
      "duration": 7.879
    },
    {
      "text": "The Pediatrics article was just the latest\nin a decades-long march of studies showing",
      "start": 55.609,
      "duration": 5.16
    },
    {
      "text": "spanking -- defined as hitting with an open\nhand in order to correct or punish -- to be",
      "start": 60.769,
      "duration": 5.161
    },
    {
      "text": "ineffective at best and psychologically harmful\nat worst.",
      "start": 65.93,
      "duration": 6.78
    },
    {
      "text": "In April, an article in the Canadian Medical\nAssociation Journal analyzed two decades of",
      "start": 72.71,
      "duration": 4.44
    },
    {
      "text": "data and concluded that spanking has no upside,\nand its downsides include increased risk for",
      "start": 77.15,
      "duration": 6.33
    },
    {
      "text": "depression, anxiety, substance abuse and aggressive\nbehavior later in life.",
      "start": 83.48,
      "duration": 8.77
    },
    {
      "text": "A few years earlier, another Pediatrics study,\nthis one by researchers at Tulane University,",
      "start": 92.25,
      "duration": 5.7
    },
    {
      "text": "concluded that children who are spanked as\noften as twice a month at age 3 are twice",
      "start": 97.95,
      "duration": 4.58
    },
    {
      "text": "as likely to become aggressive, destructive\nand mean when they are 5.",
      "start": 102.53,
      "duration": 9.39
    },
    {
      "text": "And it has been a decade since Columbia University\npsychologists went through more than 80 studies",
      "start": 111.92,
      "duration": 4.74
    },
    {
      "text": "over 62 years and found that there was a \"strong\ncorrelation\" between parents who used \"corporal",
      "start": 116.66,
      "duration": 6.19
    },
    {
      "text": "punishment\" and children who demonstrated\n11 measurable childhood behaviors.",
      "start": 122.85,
      "duration": 5.11
    },
    {
      "text": "Ten of the behaviors were negative, including\nsuch things as increased aggression and increased",
      "start": 127.96,
      "duration": 6.009
    },
    {
      "text": "antisocial behavior.",
      "start": 133.969,
      "duration": 2.701
    },
    {
      "text": "Only one could be considered positive -- spanking\ndid result in \"immediate compliance.\"",
      "start": 136.67,
      "duration": 11.34
    },
    {
      "text": "So would pointing a gun in their general direction.",
      "start": 148.01,
      "duration": 2.66
    },
    {
      "text": "But that does not make it the right thing\nto do.",
      "start": 150.67,
      "duration": 2.73
    },
    {
      "text": "And, as other research points out, if that\ntemporary compliance comes at the price of",
      "start": 153.4,
      "duration": 5.979
    },
    {
      "text": "long-term depression or defiance, then what\nhas really been gained?",
      "start": 159.379,
      "duration": 8.041
    },
    {
      "text": "In spite of this mountain of data, though,\npolls and studies find that up to 90 percent",
      "start": 167.42,
      "duration": 4.39
    },
    {
      "text": "of parents spank their children.",
      "start": 171.81,
      "duration": 5.08
    },
    {
      "text": "And each time we parenting reporters write\nabout the latest studies, our comment threads",
      "start": 176.89,
      "duration": 4.12
    },
    {
      "text": "fill with practitioners, whose remarks range\nfrom outrage (\"I was hit and I turned out",
      "start": 181.01,
      "duration": 5.33
    },
    {
      "text": "okay god damn it\") to despair (\"I don't want\nto hit, but it is the only way I can get them",
      "start": 186.34,
      "duration": 7.399
    },
    {
      "text": "to listen\").",
      "start": 193.739,
      "duration": 1
    },
    {
      "text": "(You can get the idea here...)",
      "start": 194.739,
      "duration": 2.47
    },
    {
      "text": "I am continually amazed at what it takes to\nredirect parenting opinion.",
      "start": 197.209,
      "duration": 5.78
    },
    {
      "text": "It is dizzying how quickly one study or article\ncan -- sometimes -- change our ways.",
      "start": 202.989,
      "duration": 7.86
    },
    {
      "text": "We started placing infants on their backs\nrather than their stomachs when there were",
      "start": 210.849,
      "duration": 3.601
    },
    {
      "text": "hints of correlation, but not proof of causation,\nwith crib death.",
      "start": 214.45,
      "duration": 5.569
    },
    {
      "text": "Pregnant women stopped having sushi, soft\ncheese, caffeine and even a sip of alcohol",
      "start": 220.019,
      "duration": 4.601
    },
    {
      "text": "on the remote but striking possibility that\na small amount could have consequences.",
      "start": 224.62,
      "duration": 5.78
    },
    {
      "text": "BPA bottles disappeared in certain circles\novernight when there was an unofficial link",
      "start": 230.4,
      "duration": 7.25
    },
    {
      "text": "to cancer.",
      "start": 237.65,
      "duration": 3.169
    },
    {
      "text": "But other times, we just don't want to know.",
      "start": 240.819,
      "duration": 3.98
    },
    {
      "text": "In that way the spanking conversation is like\nthe vaccine \"debate.\"",
      "start": 244.799,
      "duration": 3.931
    },
    {
      "text": "In spite of no credible evidence of a link\nwith autism, and many studies that tried and",
      "start": 248.73,
      "duration": 3.979
    },
    {
      "text": "failed to find such a link, there are some\nminds that just won't change.",
      "start": 252.709,
      "duration": 6.43
    },
    {
      "text": "Your parents hit you, and you are okay?",
      "start": 259.139,
      "duration": 7.281
    },
    {
      "text": "They probably smoked around you, too, and\nthey didn't make you wear a seatbelt, either,",
      "start": 266.42,
      "duration": 3.56
    },
    {
      "text": "but we know better now.",
      "start": 269.98,
      "duration": 2.92
    },
    {
      "text": "Also, might I respectfully ask how you know\nthat you're okay?",
      "start": 272.9,
      "duration": 6.97
    },
    {
      "text": "Perhaps if your parents hadn't hit their kids,\nyou wouldn't feel a need to hit your own?",
      "start": 279.87,
      "duration": 8.57
    },
    {
      "text": "It is the only thing that works when your\nchildren won't listen?",
      "start": 288.44,
      "duration": 3.91
    },
    {
      "text": "Swedish children are not running amok in the\nstreets, and spanking has been illegal there",
      "start": 292.35,
      "duration": 4.35
    },
    {
      "text": "since 1979.",
      "start": 296.7,
      "duration": 3.37
    },
    {
      "text": "Sweden was the first of 32 countries -- including\nCosta Rica, Israel, Kenya and most of Europe",
      "start": 300.07,
      "duration": 6.15
    },
    {
      "text": "-- to approve such a law.",
      "start": 306.22,
      "duration": 4.78
    },
    {
      "text": "Some questions really don't have two sides.",
      "start": 311,
      "duration": 3.44
    },
    {
      "text": "\"Is it okay to do something to your child\nthat would land you in jail if you did it",
      "start": 314.44,
      "duration": 4.13
    },
    {
      "text": "to a stranger on the street?\" is one of those.",
      "start": 318.57,
      "duration": 5.28
    },
    {
      "text": "You can phrase it other ways too -- like \"Is\nit okay to hurt a child because it serves",
      "start": 323.85,
      "duration": 6.03
    },
    {
      "text": "your immediate goal when science shows it\ncan lead to long-term harm?\"",
      "start": 329.88,
      "duration": 5.36
    },
    {
      "text": "But there is still just one answer.",
      "start": 335.24,
      "duration": 3.58
    },
    {
      "text": "And yet, we keep seeing it presented as a\ndisagreement.",
      "start": 338.82,
      "duration": 4.82
    },
    {
      "text": "\"To Spank or Not to Spank\" was the headline\non both the CNN's report yesterday and the",
      "start": 343.64,
      "duration": 5.75
    },
    {
      "text": "\"Good Morning America\" segment on Thursday\nabout the latest Pediatrics study.",
      "start": 349.39,
      "duration": 4.52
    },
    {
      "text": "The \"Today\" piece added the tagline: \"Mommy\nWars: Raging Parenting Debate,\" and a Babble",
      "start": 353.91,
      "duration": 5.12
    },
    {
      "text": "blogger was found to represent each side.",
      "start": 359.03,
      "duration": 6.33
    },
    {
      "text": "But there aren't two sides.",
      "start": 365.36,
      "duration": 3.16
    },
    {
      "text": "There is a preponderance of fact, and there\nare people who find it inconvenient to accept",
      "start": 368.52,
      "duration": 5.81
    },
    {
      "text": "those facts.",
      "start": 374.33,
      "duration": 2.24
    },
    {
      "text": "Where, exactly is the debate?",
      "start": 376.57,
      "duration": 4.73
    }
  ],
  "Errors": null,
  "TranscriptStderr": null
}
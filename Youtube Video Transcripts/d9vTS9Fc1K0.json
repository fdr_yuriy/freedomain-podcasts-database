{
  "Title": "How To Fight Police Brutality!",
  "Id": "d9vTS9Fc1K0",
  "Transcript": [
    {
      "text": "It is very tempting to spend one’s anger\nand outrage on particular manifestations of",
      "start": 0.26,
      "duration": 7
    },
    {
      "text": "an unjust system – but all that does is\npretend that the rest of the system is somehow",
      "start": 7.42,
      "duration": 7
    },
    {
      "text": "just.\nSome of my listeners are upset that I do not",
      "start": 14.45,
      "duration": 5.55
    },
    {
      "text": "rail against what they perceive as specific\npolice abuses, because they do not understand",
      "start": 20,
      "duration": 6.35
    },
    {
      "text": "how to oppose an unjust system as a whole.\nIn the Soviet Union, the people’s frustration",
      "start": 26.35,
      "duration": 7
    },
    {
      "text": "and rage was regularly directed at supposed\nsaboteurs and counterrevolutionaries and other",
      "start": 34.6,
      "duration": 6.059
    },
    {
      "text": "ideological criminals through show trials,\nwhich allowed the people to focus their anger",
      "start": 40.659,
      "duration": 5.38
    },
    {
      "text": "on particular individuals, rather than the\nsystem as a whole.",
      "start": 46.039,
      "duration": 5.881
    },
    {
      "text": "In a democracy, show trials remain necessary,\nof course, but democracies have the option",
      "start": 51.92,
      "duration": 7
    },
    {
      "text": "of focusing the peoples rage on particular\nleaders – George Bush or Barack Obama, it",
      "start": 58.989,
      "duration": 6.201
    },
    {
      "text": "doesn’t really matter – so that everyone\ncan indulge in the fantasy that, although",
      "start": 65.19,
      "duration": 5.38
    },
    {
      "text": "political leaders are supposed to be bound\nby the Constitution and the rule of law, having",
      "start": 70.57,
      "duration": 4.15
    },
    {
      "text": "a new leader will somehow magically change\nthe system. This is like believing that a",
      "start": 74.72,
      "duration": 6.35
    },
    {
      "text": "new basketball coach will grant a team the\nability to fly.",
      "start": 81.07,
      "duration": 7
    },
    {
      "text": "And so, in grim propagandistic repetition,\nwe see show trials regularly erupting regarding",
      "start": 89.35,
      "duration": 7
    },
    {
      "text": "police brutality, political corruption, conflicts\nof interest, affairs, private email servers,",
      "start": 97.53,
      "duration": 6.46
    },
    {
      "text": "public foundations – you name it, your attention\nis drawn to these public circuses.",
      "start": 103.99,
      "duration": 7
    },
    {
      "text": "In magic, this is called “misdirection,”\nwhich is the art of drawing the attention",
      "start": 112.51,
      "duration": 3.55
    },
    {
      "text": "of the audience away from the hand actually\nperforming the trick.",
      "start": 116.06,
      "duration": 4.59
    },
    {
      "text": "In the black magic of politics, this is called\n“the media.”",
      "start": 120.65,
      "duration": 7
    },
    {
      "text": "Intelligent and alert people ignore the show\ntrials, but rather look for what the show",
      "start": 130.81,
      "duration": 3.87
    },
    {
      "text": "trials are designed to obscure.\nYou cannot see the stars when the sun is out,",
      "start": 134.68,
      "duration": 7
    },
    {
      "text": "unless the moon covers the sun, at which point\nthe constellations become visible.",
      "start": 143.579,
      "duration": 6.88
    },
    {
      "text": "When you cover the air horn blaring of the\nmedia circus, the true constellations of political",
      "start": 150.459,
      "duration": 5.54
    },
    {
      "text": "power become immediately visible.\nWhen you find yourself asking whether the",
      "start": 155.999,
      "duration": 6.14
    },
    {
      "text": "money taken in by Bill Clinton’s foundation\ninfluenced his wife’s decisions while she",
      "start": 162.139,
      "duration": 4.68
    },
    {
      "text": "was Secretary of State, you can ignore the\nreality that almost all political donations",
      "start": 166.819,
      "duration": 7
    },
    {
      "text": "are paid to influence the power of the state\n– or that 0.01% of the American public provide",
      "start": 173.879,
      "duration": 7
    },
    {
      "text": "close to half of all political donations.\nThe 1% of the 1% effectively control the awesome",
      "start": 181.06,
      "duration": 7
    },
    {
      "text": "power of the modern political state.\nIt is only by ignoring the reality that the",
      "start": 188.859,
      "duration": 7
    },
    {
      "text": "rich buy political favours - that the $1.5\nbillion it takes to run a presidential campaign",
      "start": 198.719,
      "duration": 5.72
    },
    {
      "text": "is paid for by people who will get far more\nthan $1.5 billion out of value out of their",
      "start": 204.439,
      "duration": 5.92
    },
    {
      "text": "bought and paid for president - it is only\nby ignoring this, that you can describe the",
      "start": 210.359,
      "duration": 4.8
    },
    {
      "text": "system as a republic, or a democracy, without\nrolling your eyes.",
      "start": 215.159,
      "duration": 7
    },
    {
      "text": "When the media gets you to focus on bad apples,\nby implication the remaining apples must be",
      "start": 222.76,
      "duration": 4.92
    },
    {
      "text": "good – and that is the purpose of media\nattention: to validate the system as a whole",
      "start": 227.68,
      "duration": 6.139
    },
    {
      "text": "by serving up “exceptions.”\nIf you are told that only one student out",
      "start": 233.819,
      "duration": 6.28
    },
    {
      "text": "of a class of 500 was caught cheating – and\npunished – you are being unconsciously programmed",
      "start": 240.099,
      "duration": 5.25
    },
    {
      "text": "to regard the other 499 students as honest.\nExceptions reinforce the perception of standards.",
      "start": 245.349,
      "duration": 7
    },
    {
      "text": "Where are Lois Lerner’s emails?\nShould there be an IRS at all?",
      "start": 257.19,
      "duration": 5.65
    },
    {
      "text": "Did Hillary Clinton facilitate the transfer\nof 20% of America’s uranium to Russian control?",
      "start": 262.84,
      "duration": 6.48
    },
    {
      "text": "Should the government have that power at all?\nThe government is wasting money.",
      "start": 269.32,
      "duration": 5.57
    },
    {
      "text": "Should the government control money at all?\nWhy is the media focusing on police violence",
      "start": 274.89,
      "duration": 7
    },
    {
      "text": "against blacks?\nBecause elections, and welfare.",
      "start": 285.28,
      "duration": 5.38
    },
    {
      "text": "The media is left-wing, and the Democrats\nare left-wing.",
      "start": 290.66,
      "duration": 5.66
    },
    {
      "text": "Just look at Baltimore – a dying city that\nhas been controlled exclusively by Democrats",
      "start": 296.32,
      "duration": 5.05
    },
    {
      "text": "since 1967 – almost a half-century. The\nmayor, police, city Council and other officials",
      "start": 301.37,
      "duration": 7
    },
    {
      "text": "are all liberal democrats. The Maryland state\nlegislature is controlled by Democrats; they",
      "start": 309.45,
      "duration": 5.12
    },
    {
      "text": "have had a Democrat Governor for the past\neight years, and out of eight congressional",
      "start": 314.57,
      "duration": 4.32
    },
    {
      "text": "districts, six are controlled by Democrats.\nThe Democrats control the school system that",
      "start": 318.89,
      "duration": 5.31
    },
    {
      "text": "has massive failure rates, despite ranking\nin the top five among the nation’s 100 largest",
      "start": 324.2,
      "duration": 5.79
    },
    {
      "text": "school districts in spending per pupil. Democrats\nconsistently oppose school vouchers – which",
      "start": 329.99,
      "duration": 5.85
    },
    {
      "text": "many African-American parents really want\n– in order to keep the donations flowing",
      "start": 335.84,
      "duration": 3.76
    },
    {
      "text": "from their union cronies.\nLook at Detroit – controlled by Democrats",
      "start": 339.6,
      "duration": 5.55
    },
    {
      "text": "since 1961.\nThe American government spends almost $1 trillion",
      "start": 345.15,
      "duration": 7
    },
    {
      "text": "a year fighting poverty – over $61,000 per\npoor family – and fails.",
      "start": 352.15,
      "duration": 7
    },
    {
      "text": "Since President Obama was inaugurated, federal\nwelfare spending has increased by 41%.",
      "start": 360.93,
      "duration": 7
    },
    {
      "text": "Since Lyndon Johnson – a Democrat – declared\nthe war on poverty in 1964, the poverty rate",
      "start": 369.65,
      "duration": 5.53
    },
    {
      "text": "has barely budged. In real terms, poverty\nis almost infinitely worse, because we now",
      "start": 375.18,
      "duration": 7
    },
    {
      "text": "have persistent, endemic poverty – and also,\na crippling national debt, and utterly unpayable",
      "start": 382.61,
      "duration": 7
    },
    {
      "text": "unfunded liabilities. Being poor is one thing\n– being poor and in over $1.2 million in",
      "start": 390.66,
      "duration": 7
    },
    {
      "text": "debt is quite another. You can work your way\nout of poverty – you cannot dig your way",
      "start": 400.5,
      "duration": 7
    },
    {
      "text": "out of that kind of debt.\nThe welfare state is worse than a failure",
      "start": 409.03,
      "duration": 7
    },
    {
      "text": "– it is a soul-destroying, economy-shredding,\npoisoned poverty hold that has killed families,",
      "start": 416.83,
      "duration": 7
    },
    {
      "text": "murdered neighbourhoods and slaughtered opportunity\nfor more than two generations now.",
      "start": 424.12,
      "duration": 7
    },
    {
      "text": "Sure, and the big problem for the poor – particularly\npoor Blacks – are a few rogue cops.",
      "start": 432.17,
      "duration": 7
    },
    {
      "text": "We are now in danger of forgetting what life\nwas like before welfare – let me remind",
      "start": 443.94,
      "duration": 7
    },
    {
      "text": "you of what once was, and the birth of opportunity\nthat was killed in the crib.",
      "start": 451.59,
      "duration": 7
    },
    {
      "text": "Between 1950 in 1965, poverty was cut in half\nin America. Between 1940 and 1960, the poverty",
      "start": 460.5,
      "duration": 7
    },
    {
      "text": "rate for Blacks had been cut nearly in half.\nBetween 1936 and 1959, for various skilled",
      "start": 472.62,
      "duration": 7
    },
    {
      "text": "trades, the incomes of Blacks relative to\nwhites had more than doubled. The number of",
      "start": 482.31,
      "duration": 7
    },
    {
      "text": "Blacks in professional and other high-level\noccupations grew more quickly during the half",
      "start": 491.4,
      "duration": 4.8
    },
    {
      "text": "decade before the launch of the welfare state\nthat during the five years after it began.",
      "start": 496.2,
      "duration": 7
    },
    {
      "text": "The welfare state punished marriage, punished\nwork – if a woman on welfare married a man",
      "start": 505.28,
      "duration": 7
    },
    {
      "text": "working a low-paying job, a few extra dollars\nin income costs thousands of dollars in benefits.",
      "start": 512.74,
      "duration": 7
    },
    {
      "text": "The two most essential roots out of poverty\n– marriage and work – are crushingly taxed",
      "start": 521.279,
      "duration": 6.471
    },
    {
      "text": "in the welfare state.\nThis is one reason why out of wedlock births",
      "start": 527.75,
      "duration": 5.9
    },
    {
      "text": "in the black community have tripled – to\nalmost 75% – since the beginning of the",
      "start": 533.65,
      "duration": 4.84
    },
    {
      "text": "welfare state. Fully 50% of that disintegration\ncomes directly from the perverse incentives",
      "start": 538.49,
      "duration": 7
    },
    {
      "text": "of the welfare state.\nThe National Association for the Advancement",
      "start": 548.24,
      "duration": 6.83
    },
    {
      "text": "of Colored People reported that \"the ready\naccess to a lifetime of welfare and free social",
      "start": 555.07,
      "duration": 6.85
    },
    {
      "text": "service programs is a major contributory factor\nto the crime problems we face today.\"",
      "start": 561.92,
      "duration": 7
    },
    {
      "text": "A 50 percent increase in the monthly value\nof combined AFDC and food stamp benefits led",
      "start": 571.94,
      "duration": 7
    },
    {
      "text": "to a 117% increase in crime rates among young\nblack men.",
      "start": 579.48,
      "duration": 7
    },
    {
      "text": "Barbara Whitehead:\n“The relationship [between single-parent",
      "start": 591.77,
      "duration": 6
    },
    {
      "text": "families and crime] is so strong that controlling\nfor family configuration erases the relationship",
      "start": 597.77,
      "duration": 6.68
    },
    {
      "text": "between race and crime and between low income\nand crime. This conclusion shows up time and",
      "start": 604.45,
      "duration": 6.54
    },
    {
      "text": "again in the literature. The nation's mayors,\nas well as police officers, social workers,",
      "start": 610.99,
      "duration": 5.47
    },
    {
      "text": "probation officers, and court officials, consistently\npoint to family breakup as the most important",
      "start": 616.46,
      "duration": 7
    },
    {
      "text": "source of rising rates of crime.”\nWelfare recipients are not lazy – they are",
      "start": 623.95,
      "duration": 7
    },
    {
      "text": "rational. Low-paying jobs are pretty terrible\n- if you are working as a bus boy in a busy",
      "start": 634.75,
      "duration": 7
    },
    {
      "text": "restaurant, hustling and sweating and being\nyelled at all day long, until your feet ache",
      "start": 642.33,
      "duration": 4.37
    },
    {
      "text": "- and your boss takes you aside and tells\nyou that he will pay you double your salary",
      "start": 646.7,
      "duration": 4.09
    },
    {
      "text": "to stay home, would you come to work the next\nday?",
      "start": 650.79,
      "duration": 7
    },
    {
      "text": "What happens to kids on welfare?\nDr. June O'Neill and Anne Hill, comparing",
      "start": 658.05,
      "duration": 6.28
    },
    {
      "text": "children who were identical in terms of such\nsocial and economic factors as race, family",
      "start": 664.33,
      "duration": 5.28
    },
    {
      "text": "structure, neighborhood, family income, and\nmothers' IQ and education, found that the",
      "start": 669.61,
      "duration": 4.39
    },
    {
      "text": "more years a child spent on welfare, the lower\nthe child's IQ. The more welfare income received",
      "start": 674,
      "duration": 7
    },
    {
      "text": "by family, the lower a son’s earnings as\nan adult.",
      "start": 682.55,
      "duration": 7
    },
    {
      "text": "“Throughout the epoch of slavery and into\nthe early decades of the twentieth century,",
      "start": 690.15,
      "duration": 6.58
    },
    {
      "text": "most black children grew up in two-parent\nhouseholds.¨ Post-Civil War studies revealed",
      "start": 696.73,
      "duration": 7
    },
    {
      "text": "that most black couples in their forties had\nbeen together for at least twenty years. In",
      "start": 703.93,
      "duration": 6.49
    },
    {
      "text": "southern urban areas around 1880, nearly three-fourths\nof black households were husband-or father-present;",
      "start": 710.42,
      "duration": 7
    },
    {
      "text": "in southern rural settings, the figure approached\n86%. As of 1940, the illegitimacy rate among",
      "start": 718,
      "duration": 7
    },
    {
      "text": "blacks nationwide was approximately 15%—scarcely\none-fifth of the current figure. As late as",
      "start": 726.029,
      "duration": 7
    },
    {
      "text": "1950, black women were more likely to be married\nthan white women, and only 9% of black families",
      "start": 733.48,
      "duration": 5.52
    },
    {
      "text": "with children were headed by a single parent.”\n“During the nine decades between the Emancipation",
      "start": 739,
      "duration": 7
    },
    {
      "text": "Proclamation and the 1950s, the black family\nremained a strong, stable institution. Its",
      "start": 747.23,
      "duration": 7
    },
    {
      "text": "cataclysmic destruction was subsequently set\nin motion by such policies as the anti-marriage",
      "start": 754.37,
      "duration": 5.07
    },
    {
      "text": "incentives that are built into the welfare\nsystem have served only to exacerbate the",
      "start": 759.44,
      "duration": 4.68
    },
    {
      "text": "problem. As George Mason University professor\nWalter E. Williams puts it: “The welfare",
      "start": 764.12,
      "duration": 5.63
    },
    {
      "text": "state has done to black Americans what slavery\ncouldn't do, what Jim Crow couldn't do, what",
      "start": 769.75,
      "duration": 5.95
    },
    {
      "text": "the harshest racism couldn't do. And that\nis to destroy the black family.” Hoover",
      "start": 775.7,
      "duration": 7
    },
    {
      "text": "Institution Fellow Thomas Sowell concurs:\n“The black family, which had survived centuries",
      "start": 782.899,
      "duration": 5.291
    },
    {
      "text": "of slavery and discrimination, began rapidly\ndisintegrating in the liberal welfare state",
      "start": 788.19,
      "duration": 5.05
    },
    {
      "text": "that subsidized unwed pregnancy and changed\nwelfare from an emergency rescue to a way",
      "start": 793.24,
      "duration": 5.909
    },
    {
      "text": "of life.”\nThe entire Democratic apparatus in the United",
      "start": 799.149,
      "duration": 7
    },
    {
      "text": "States is set up to buy votes with stolen\nor printed money. If the welfare state is",
      "start": 807.76,
      "duration": 7
    },
    {
      "text": "threatened, half the political power in the\nUnited States goes up in smoke.",
      "start": 817.56,
      "duration": 7
    },
    {
      "text": "The evidence is accumulating, the case is\nno longer open to reasonable doubt. The American",
      "start": 826.41,
      "duration": 7
    },
    {
      "text": "government is going bankrupt buying votes\n– as the money runs out, new enemies need",
      "start": 835.47,
      "duration": 7
    },
    {
      "text": "to be invented.\nRepublicans passed the Thirteenth Amendment",
      "start": 843.79,
      "duration": 7
    },
    {
      "text": "ending slavery – and 80% of Democrats voted\nagainst it.",
      "start": 851.43,
      "duration": 5.99
    },
    {
      "text": "Republicans who unanimously enacted the Fourteenth\nAmendment, granting freed slaves the rights",
      "start": 857.42,
      "duration": 4.96
    },
    {
      "text": "of citizenship – with unanimous opposition\nby Democrats.",
      "start": 862.38,
      "duration": 6.579
    },
    {
      "text": "Republicans passed the Fifteenth Amendment,\ngiving blacks the right to vote.",
      "start": 868.959,
      "duration": 6.911
    },
    {
      "text": "Republicans tried to pass anti-lynching laws,\nwhich were staunchly opposed by Democrats.",
      "start": 875.87,
      "duration": 7
    },
    {
      "text": "The Ku Klux Klan was the military arm of the\nDemocratic Party.",
      "start": 883.089,
      "duration": 5.761
    },
    {
      "text": "The Democrats fought the Civil War in the\nhopes of continuing the enslavement of black",
      "start": 888.85,
      "duration": 3.82
    },
    {
      "text": "people – Democrats subjected semi-free Blacks\nto Jim Crow laws and Ku Klux Klan violence.",
      "start": 892.67,
      "duration": 7
    },
    {
      "text": "Democrats fought hard for segregationist laws,\nanti-Black gun restrictions, anti-Black Union",
      "start": 901.92,
      "duration": 5.12
    },
    {
      "text": "powers, anti-black minimum wages, you name\nit.",
      "start": 907.04,
      "duration": 4.25
    },
    {
      "text": "In 1956, 99 members of Congress signed a “Southern\nmanifesto” denouncing desegregation – 2",
      "start": 911.29,
      "duration": 7
    },
    {
      "text": "were Republicans, 97 were Democrats. Lyndon\nB. Johnson – a Democrat – stripped Republican",
      "start": 920.83,
      "duration": 7
    },
    {
      "text": "Eisenhower’s 1957 civil rights bill of its\nenforcement provisions. The bill was later",
      "start": 929.85,
      "duration": 5.549
    },
    {
      "text": "opposed by 18 Democrat senators. Every single\nSenate segregationist was a Democrat.",
      "start": 935.399,
      "duration": 7
    },
    {
      "text": "LBJ reversed his position on civil rights,\nsponsoring the 1964 Civil Rights Act, and",
      "start": 946.25,
      "duration": 5.279
    },
    {
      "text": "reportedly explained to his fellow Democrats,\n“I’ll have them niggers voting Democrat",
      "start": 951.529,
      "duration": 5.511
    },
    {
      "text": "for two hundred years.”\nAnd thus, the rise and advance of historically",
      "start": 957.04,
      "duration": 7
    },
    {
      "text": "oppressed population was first arrested, and\nthen reversed.",
      "start": 965.17,
      "duration": 7
    },
    {
      "text": "The goal of the media is to focus people’s\nresentment on the police, rather than the",
      "start": 976.31,
      "duration": 7
    },
    {
      "text": "welfare state. The police are responding to\nthe rise in crime where single motherhood",
      "start": 983.55,
      "duration": 5.99
    },
    {
      "text": "is most prevalent. However, rather than talk\nabout how the welfare state destroys the family,",
      "start": 989.54,
      "duration": 6.57
    },
    {
      "text": "the economy, and entire communities – and\nthe future – the media want to talk about",
      "start": 996.11,
      "duration": 5.03
    },
    {
      "text": "police aggression. Thus, anyone who wants\nto talk about the root causes of violence",
      "start": 1001.14,
      "duration": 6.6
    },
    {
      "text": "and oppression, to avoid surface symptoms\nand deal with what is really happening, and",
      "start": 1007.74,
      "duration": 4.67
    },
    {
      "text": "why – can be called a racist cop apologist,\nand dismissed from the pretense of a debate.",
      "start": 1012.41,
      "duration": 7
    },
    {
      "text": "I grew up in a poor neighbourhood. I’m the\nson of a single mother. My friends were the",
      "start": 1021.01,
      "duration": 7
    },
    {
      "text": "children of single mothers. I have seen the\neffects. I know this world, this underworld.",
      "start": 1031.809,
      "duration": 7
    },
    {
      "text": "If you really want to reduce police abuse,\nabolish the welfare state. There is in fact",
      "start": 1044.949,
      "duration": 7
    },
    {
      "text": "no other way.",
      "start": 1056.34,
      "duration": 0.7
    }
  ],
  "Errors": null,
  "TranscriptStderr": null
}